package common;

import com.codeborne.selenide.WebDriverRunner;
import components.SlideContent;
import components.modals.AddPatientModal;
import components.modals.ConfirmationModal;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Cookie;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import pages.*;
import pages.apps.GuidelinesPage;
import pages.apps.help_and_more.AnalyticsPage;
import utils.SelenideConfiguration;
import utils.api.meeting.MeetingApi;
import utils.api.patient.PatientApi;
import utils.api.zephyr.ZephyrReporter;
import utils.config.EnvData;
import utils.config.enums.UserType;
import utils.report.Reporter;

import java.util.Calendar;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

@Slf4j
@Listeners(TestListener.class)
public class Common {
    static {
        SelenideConfiguration.setUp();
        Reporter.createExtentReportsInstance();
    }

    protected final HeadBar headBar = new HeadBar();
    protected GuidelinesPage guidelinesPage;
    protected AddPatientModal addPatientModal;
    protected AnalyticsPage analyticsPage;
    protected ConfirmationModal confirmationModal;
    protected CancerInfo cancerInfo;
    protected PatientMenu patientMenu;

    protected PatientEventTimeline patientEventTimeline;
    protected PatientEventTimelineDetails patientEventTimelineDetails;

    protected FullScreenPreview fullScreenPreview;
    protected MeetingPresentation meetingPresentation;
    protected SlideContent slideContent;

    @BeforeSuite
    public void setUpSuite() {
        if (!EnvData.getZephyrEnable()) {
            log.info("Zephyr integration is disabled");
        } else {
            log.info("Zephyr integration is enabled");
            ZephyrReporter.generateBasicData();
        }
    }

    @AfterClass
    public void tearDownClass() {
        closeWebDriver();

        EnvData.getUsers().forEach(user -> {
            EnvData.setActiveUser(user);
            user.resources.getPatientList().forEach(patient -> new PatientApi().delete(patient.getUuid())
            );

            user.resources.getMeetingList().forEach(meeting -> new MeetingApi().delete(meeting.getUuid())
            );
        });
    }

    @AfterSuite
    public void tearDownSuite() {
        if (EnvData.getZephyrEnable()) {
            ZephyrReporter.reportAll();
        }
        Reporter.getExtentReports().flush();
    }

    protected static void loginToTumorBoard(UserType userType) {
        EnvData.setActiveUser(userType);
        open(EnvData.getActiveUser().getUrl());

        final var calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 5);

        final var cookie = new Cookie("access_token", EnvData.getActiveUser().getToken().substring(7), ".platform.navify.com", "/", calendar.getTime());

        WebDriverRunner.getWebDriver().manage().addCookie(cookie);
        refresh();

        $("app-meeting-patient-list").shouldBe(visible);
    }

    protected void switchToAppFrame() {
        $("iframe[title='CDS App Plugin']").shouldBe(exist, visible);
        switchTo().frame(0);
    }
}
