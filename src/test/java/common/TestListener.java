package common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.OutputType;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.api.zephyr.ZephyrReporter;
import utils.api.zephyr.models.tc.Step;
import utils.api.zephyr.models.tc.Test;
import utils.report.Reporter;

import java.util.ArrayList;
import java.util.List;

public class TestListener implements ITestListener {
    private ExtentTest extentTest;
    private List<Step> stepList;

    public void onTestSuccess(ITestResult result) {
        final var screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        extentTest.createNode(result.getMethod().getDescription()).pass(MediaEntityBuilder.createScreenCaptureFromBase64String(screenshotAsBase64).build());
        final var step = Step.builder()
                .name(result.getMethod().getDescription())
                .result(result.getStatus())
                .screenshot(screenshotAsBase64)
                .build();
        stepList.add(step);
    }

    public void onTestFailure(ITestResult result) {
        final var screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        extentTest.createNode(result.getMethod().getDescription()).fail(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromBase64String(screenshotAsBase64).build());
        final var step = Step.builder()
                .name(result.getMethod().getDescription())
                .result(result.getStatus())
                .screenshot(screenshotAsBase64)
                .errorDescription(result.getThrowable().toString())
                .build();
        stepList.add(step);
    }

    public void onStart(ITestContext context) {
        extentTest = Reporter.getExtentReports().createTest(context.getName());
        stepList = new ArrayList<>();
    }

    public void onFinish(ITestContext context) {
        final var result = context.getFailedTests().size() == 0 ? 1 : 2;

        final var test = Test.builder()
                .testName(context.getName())
                .tag(ZephyrReporter.getTag(context.getName()))
                .result(result)
                .steps(stepList)
                .build();

        ZephyrReporter.addTestToSuit(test);
    }
}
