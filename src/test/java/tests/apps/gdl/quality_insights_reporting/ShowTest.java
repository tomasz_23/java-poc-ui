package tests.apps.gdl.quality_insights_reporting;

import common.Common;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.HeadBar;
import pages.apps.GuidelinesPage;
import pages.apps.help_and_more.AnalyticsPage;
import utils.api.patient.PatientApi;
import utils.api.patient.model.request.PatientRequestBody;
import utils.config.enums.UserType;

import java.util.UUID;

import static com.codeborne.selenide.Condition.visible;

//https://navifypoc.atlassian.net/browse/GDL-2604
public class ShowTest extends Common {
    private final HeadBar headBar = new HeadBar();
    private GuidelinesPage guidelinesPage;
    private AnalyticsPage analyticsPage;

    private PatientApi patientApi;
    private PatientRequestBody patientRequestBody;

    private UUID patientUUID;
    private int breastCancerValue;

    @BeforeClass
    public void setUp() {
        loginToTumorBoard(UserType.LOGIN);
//        patientApi = new PatientApi(activeUser);
//        patientRequestBody = PatientFactory.getBasicPatient(activeUser.getFacilityId());
//        patientUUID = patientApi.createPatient(patientRequestBody);
    }

    @Test(description = "Step 01 - Login as default user.")
    public void step01() {
        headBar.userName.shouldBe(visible);
    }

//    @Test(description = "Step 02 - Launch Guidelines from 'Apps' menu.")
//    public void step02() {
//        guidelinesPage = (GuidelinesPage) headBar.goToApp(Apps.GDL);
//        switchToAppFrame();
//        guidelinesPage.component.shouldBe(visible);
//    }
//
//    @Test(description = "Step 03 - Click 'help & more' link and select 'Analytics'.")
//    public void step03() {
//        guidelinesPage.helpAndMore.click();
//        analyticsPage = (AnalyticsPage) guidelinesPage.selectHelpAndMoreOption(HelpAndMore.ANALYTICS);
//
//        analyticsPage.barChart.shouldBe(visible);
//        analyticsPage.gdlAnalyticsPathwaysByCancerListElements.get(0).shouldBe(visible);
//    }
//
//    @Test(description = "Step 04 - Check 'Clinical pathways by cancer type:' section.")
//    public void step04() {
//        analyticsPage.filtersComponent.shouldBe(visible);
//        analyticsPage.utilizationNumber.shouldBe(visible);
//        analyticsPage.customizedPathwayReasonSection.shouldBe(visible);
//        analyticsPage.clinicalPathwaysByCancerTypeSection.shouldBe(visible);
//        analyticsPage.pathwaysCustomizationSection.shouldBe(visible);
//    }
//
//    @Test(description = "Step 05 - Hover over dark blue Breast Cancer chart.")
//    public void step05() {
//        breastCancerValue = analyticsPage.getValueFromTooltip(CancerTypePathway.BREAST_CANCER, Customization.WITHOUT);
//    }
//
//    @Test(description = "Step 06 - Launch Guidelines from Patient`s A perspective and create pathway for Breast Cancer.")
//    public void step06() {
//        WebDriverRunner.getWebDriver().switchTo().defaultContent();
//        headBar.searchPatient(patientRequestBody);
//        new PatientMenu().goToApp(Apps.GDL);
//        switchToAppFrame();
//        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.SOURCE, GuidelineSource.Source.NCCN);
//
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    //    @Test(description = "Step 03 - Click 'help & more' link and select 'Analytics'.")
//    public void goToAnalytics() {
//        guidelinesPage.
//    }
//
//    @Test(description = "Step 03 - Click 'help & more' link and select 'Analytics'.")
//    public void goToAnalytics() {
//        guidelinesPage.
//    }
//
//    @Test(description = "Step 09 - Log out from the application.")
//    public void step09() {
//        headBar.logout();
//    }

//    @AfterClass
//    public void exit() {
//        patientApi.deletePatient(patientUUID);
//    }
}