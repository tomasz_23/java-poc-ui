package tests.apps.gdl.quality_insights_reporting;

import com.codeborne.selenide.WebDriverRunner;
import common.Common;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PatientMenu;
import pages.apps.dropdownlist.CancerType;
import pages.apps.dropdownlist.GuidelineSource;
import pages.apps.dropdownlist.StartingPoint;
import utils.api.patient.PatientApi;
import utils.api.patient.PatientFactory;
import utils.api.patient.model.request.PatientRequestBody;
import utils.config.enums.UserType;
import utils.enums.CancerTypePathway;
import utils.enums.Customization;
import utils.enums.GuidelineDropdownList;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.visible;
import static org.assertj.core.api.Assertions.assertThat;

//https://navifypoc.atlassian.net/browse/GDL-2604
public class ShowClinicalPathwayByCancerTypeTest extends Common {
    private PatientRequestBody patientRequestBody;
    private int breastCancerValue;

    @BeforeClass
    public void setUp() {
        loginToTumorBoard(UserType.LOGIN);
        patientRequestBody = PatientFactory.getBasicPatient();
        new PatientApi().create(patientRequestBody);
    }

    @Test(description = "Step 01 - Login as default user.")
    public void step01() {
        headBar.userName.shouldBe(visible);
    }

    @Test(description = "Step 02 - Launch Guidelines from 'Apps' menu.")
    public void step02() {
        guidelinesPage = headBar.openAppsForm().appComponent.goToGDL();
        switchToAppFrame();
        guidelinesPage.component.shouldBe(visible);
    }

    @Test(description = "Step 03 - Click 'help & more' link and select 'Analytics'.")
    public void step03() {
        guidelinesPage.helpAndMore.click();
        analyticsPage = guidelinesPage.openAnalytics();

        analyticsPage.barChart.shouldBe(visible);
        analyticsPage.gdlAnalyticsPathwaysByCancerListElements.get(0).shouldBe(visible);
    }

    @Test(description = "Step 04 - Check 'Clinical pathways by cancer type:' section.")
    public void step04() {
        analyticsPage.filtersComponent.shouldBe(visible);
        analyticsPage.utilizationNumber.shouldBe(visible);
        analyticsPage.customizedPathwayReasonSection.shouldBe(visible);
        analyticsPage.clinicalPathwaysByCancerTypeSection.shouldBe(visible);
        analyticsPage.pathwaysCustomizationSection.shouldBe(visible);
    }

    @Test(description = "Step 05 - Hover over dark blue Breast Cancer chart.")
    public void step05() {
        breastCancerValue = analyticsPage.getValueFromTooltip(CancerTypePathway.BREAST_CANCER, Customization.WITHOUT);
    }

    @Test(description = "Step 06 - Launch Guidelines from Patient`s A perspective and create pathway for Breast Cancer.")
    public void step06() {
        WebDriverRunner.getWebDriver().switchTo().defaultContent();
        headBar.searchPatient(patientRequestBody);
        patientMenu = new PatientMenu();
        guidelinesPage = patientMenu.appComponent.goToGDL();
        switchToAppFrame();
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.SOURCE, GuidelineSource.Values.NCCN);
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.CANCER_TYPE, CancerType.Values.BREAST_CANCER);
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.STARTING_POINT, StartingPoint.Values.CLINICAL_STAGE_WORKUP);
        guidelinesPage.selectAndGetNode(0);
    }

    @Test(description = "Step 07 - Open 'Analytics' again and change 'Select Cancer Type:' dropdown value to 'Breast Cancer'.")
    public void step07() {
        guidelinesPage.helpAndMore.click();
        guidelinesPage.openAnalytics();
        analyticsPage.barChart.shouldBe(visible);
        analyticsPage.gdlAnalyticsPathwaysByCancerListElements.shouldBe(sizeGreaterThan(0));
    }

    @Test(description = "Step 08 - Click 'help & more' link and select 'Analytics'.")
    public void step08() {
        final var breastCancerUpdated = analyticsPage.getValueFromTooltip(CancerTypePathway.BREAST_CANCER, Customization.WITHOUT);
        assertThat(breastCancerValue + 1).describedAs("Value is the same !!!").isEqualTo(breastCancerUpdated);
    }

    @Test(description = "Step 09 - Log out from the application.")
    public void step09() {
        headBar.logout();
    }
}