package tests.apps.gdl.pinning_guideline;

import common.Common;
import components.modals.AddPatientModal;
import components.modals.PinModal;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PatientEventTimeline;
import pages.PatientMenu;
import pages.apps.dropdownlist.CancerType;
import pages.apps.dropdownlist.GuidelineSource;
import pages.apps.dropdownlist.StartingPoint;
import utils.api.meeting.MeetingApi;
import utils.api.meeting.MeetingFactory;
import utils.api.meeting.model.request.MeetingRequestBody;
import utils.api.patient.PatientApi;
import utils.api.patient.PatientFactory;
import utils.api.patient.model.request.PatientRequestBody;
import utils.config.enums.UserType;
import utils.enums.GuidelineDropdownList;

import static com.codeborne.selenide.CollectionCondition.itemWithText;
import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.switchTo;
import static utils.config.enums.CSSValues.COLOR;
import static utils.config.enums.CSSValues.FILL;
import static utils.config.enums.Colors.*;

//https://navifypoc.atlassian.net/browse/GDL-2568
public class PinGuidelineSingleTBTest extends Common {
    private PatientRequestBody patientRequestBody;
    private MeetingRequestBody meetingRequestBody;
    private String stepNode;

    @BeforeClass
    public void setUp() {
        loginToTumorBoard(UserType.LOGIN);
        patientRequestBody = PatientFactory.getBasicPatient();
        meetingRequestBody = MeetingFactory.getBasicFutureMeeting();

        new PatientApi().create(patientRequestBody);
        new MeetingApi().create(meetingRequestBody);
    }

    @Test(description = "Step 01 - Login as default user.")
    public void step01() {
        headBar.userName.shouldBe(visible);
    }

    @Test(description = "Step 02 - Navigate to Patients tab and click on the patient from preconditions.")
    public void step02() {
        headBar.searchPatient(patientRequestBody).component.shouldBe(visible);
    }

    @Test(description = "Step 03 - Click on Guidelines icon in the Apps section in the left side panel. " +
            "Select 'NCCN Guidelines®' as guideline source, " +
            "choose Guideline Source and Cancer Type with created pathway.")
    public void step03() {
        patientMenu = new PatientMenu();
        guidelinesPage = patientMenu.appComponent.goToGDL();
        switchToAppFrame();
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.SOURCE, GuidelineSource.Values.NCCN);
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.CANCER_TYPE, CancerType.Values.BREAST_CANCER);
        guidelinesPage.fillDropdownByScroll(GuidelineDropdownList.STARTING_POINT, StartingPoint.Values.CLINICAL_STAGE_WORKUP);

        stepNode = guidelinesPage.guidelineOverview.clickStepNodeAndGetName(0);
        guidelinesPage.guidelineOverview.pinIcon.shouldHave(cssValue(COLOR.value, WHITE.color));
    }

    @Test(description = "Step 04 - Hover over the pin icon.")
    public void step04() {
        guidelinesPage.guidelineOverview.pinIcon.hover().shouldHave(cssValue(COLOR.value, BLUE_HOVER.color));
    }

    @Test(description = "Step 05 - Click pin icon.")
    public void step05() {
        confirmationModal = guidelinesPage.guidelineOverview.clickPinIconFirstTime();
    }

    @Test(description = "Step 06 - Click 'Yes'.")
    public void step06() {
        final var expectedContent = "This patient needs to be submitted to a tumor board prior to pinning. Do you wish to add to a tumor board?";
        confirmationModal.content.shouldHave(matchText(expectedContent));
        confirmationModal.buttons.submit.click();
    }

    @Test(description = "Step 07 - Select a tumor board and Point of discussion and submit.")
    public void step07() {
        new AddPatientModal().fillForm(meetingRequestBody.getMeetingName());
    }

    @Test(description = "Step 08 - Click 'Save'.")
    public void step08() {
        new PinModal().clickSave();
        switchToAppFrame();
        guidelinesPage.guidelineOverview.pinIcon.shouldBe(cssValue(COLOR.value, BLUE_HOVER.color));
        guidelinesPage.guidelineOverview.pinIcon.shouldBe(cssValue(FILL.value, BLUE_PINNED.color));
    }

    @Test(description = "Step 09 - Click on the patient cancer info.")
    public void step09() {
        switchTo().defaultContent();
        patientMenu.cancerInfo.click();
        patientEventTimeline = new PatientEventTimeline();
        patientEventTimeline.component.shouldBe(visible);
    }

    @Test(description = "Step 10 - Click on the future tumor board on the timeline.")
    public void step10() {
        patientEventTimelineDetails = patientEventTimeline.openTimelineDetails(meetingRequestBody.getMeetingName());
        patientEventTimelineDetails.gdlAttachments.attachments.shouldHave(size(1));
    }

    @Test(description = "Step 11 - Click on the Guideline icon in attachments section.")
    public void step11() {
        fullScreenPreview = patientEventTimelineDetails.gdlAttachments.openGdlPreview();
        fullScreenPreview.slideContent.nodes.shouldBe(itemWithText(stepNode));
    }

    @Test(description = "Step 12 - Close the view. Click 'View presentation' button.")
    public void step12() {
        fullScreenPreview.close.click();
        meetingPresentation = patientEventTimelineDetails.clickViewPresentation();
    }

    @Test(description = "Step 13 - Click the slide with the pinned Guideline.")
    public void step13() {
        slideContent = meetingPresentation.expandSlideNavigation().selectSlide(2);
        slideContent.nodes.shouldBe(itemWithText(stepNode));
    }

    @Test(description = "Step 14 - Close the presentation view. Click 'Edit presentation'.")
    public void step14() {
        meetingPresentation.close.click();
        patientEventTimelineDetails = patientEventTimeline.openTimelineDetails(meetingRequestBody.getMeetingName());
        meetingPresentation = patientEventTimelineDetails.clickEditPresentation();
    }

    @Test(description = "Step 15 - Click the slide with the pinned Guideline.")
    public void step15() {
        slideContent = meetingPresentation.selectSlide(1);
        slideContent.nodes.shouldBe(itemWithText(stepNode));
    }

    @Test(description = "Step 16 - Close the presentation view.")
    public void step16() {
        meetingPresentation.close.click();
        meetingPresentation.component.shouldNotBe(visible);
    }

    @Test(description = "Step 17 - Click on Guidelines icon in the Apps section in the left side panel. " +
            "Select 'NCCN Guidelines®' as guideline source, choose Cancer Type used before.")
    public void step17() {
        patientMenu.appComponent.goToGDL();
        switchToAppFrame();
        guidelinesPage.guidelineSource.waitForValueDisplayed(GuidelineSource.Values.NCCN);
        guidelinesPage.guidelineOverview.pinIcon.shouldBe(cssValue(FILL.value, BLUE_PINNED.color));
    }

    @Test(description = "Step 18 - Click pin icon.")
    public void step18() {
        guidelinesPage.guidelineOverview.pinIcon.click();
    }

    @Test(description = "Step 19 - Unselect the tumor board and click 'Save'.")
    public void step19() {
        final var pinModal = new PinModal();
        switchTo().defaultContent();
        pinModal.meetingList.find(matchText(meetingRequestBody.getMeetingName())).click();
        pinModal.clickSave();
        switchToAppFrame();
        guidelinesPage.guidelineOverview.pinIcon.shouldHave(cssValue(COLOR.value, WHITE.color));
    }

    @Test(description = "Step 20 - Click on the patient cancer info.")
    public void step20() {
        switchTo().defaultContent();
        patientMenu.cancerInfo.click();
        patientEventTimeline = new PatientEventTimeline();
        patientEventTimeline.component.shouldBe(visible);
    }

    @Test(description = "Step 21 - Click on the future tumor board on the timeline.")
    public void step21() {
        patientEventTimelineDetails = patientEventTimeline.openTimelineDetails(meetingRequestBody.getMeetingName());
        patientEventTimelineDetails.gdlAttachments.attachments.shouldBe(size(0));
    }

    @Test(description = "Step 22 - Click 'Edit presentation'.")
    public void step22() {
        meetingPresentation = patientEventTimelineDetails.clickEditPresentation();
        meetingPresentation.slides.shouldBe(size(1));
    }

    @Test(description = "Step 23 - Log out from the application.")
    public void step23() {
        meetingPresentation.close.click();
        headBar.logout();
    }
}