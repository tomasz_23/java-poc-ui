package utils.enums;

public enum GuidelineDropdownList {
    SOURCE, CANCER_TYPE, STARTING_POINT;
}
