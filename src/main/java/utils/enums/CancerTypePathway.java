package utils.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CancerTypePathway {
    BREAST_CANCER("Breast Cancer");

    public final String value;
}
