package utils.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum MeetingAttachmentType {
    GDL("cds-app-guidelines"),
    CTM("cds-app-clinical-trial-match"),
    PUB("cds-app-publications");

    public final String value;
}