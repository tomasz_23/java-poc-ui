package utils.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HelpAndMore {
    ABOUT_THIS_APP("About this app"),
    ANALYTICS("Analytics"),
    HELP_WITH_THIS_APP("Help with this app"),
    PRIVACY_SETTINGS("Privacy settings"),
    RELEASE_HISTORY("Release history"),
    GUIDELINES_CONFIGURATION_MODULE("Guidelines Configuration Module");

    public final String value;
}
