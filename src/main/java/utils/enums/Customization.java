package utils.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Customization {
    WITH(2), WITHOUT(1);

    public final int value;
}
