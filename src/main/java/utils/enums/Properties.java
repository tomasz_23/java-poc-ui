package utils.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Properties {
    ENV("env"),
    ZEPHYR_ENABLED("zephyr_enabled"),
    ZEPHYR_PROJECT_ID("zephyr_project_id"),
    ZEPHYR_VERSION_ID("zephyr_version_id"),
    ZEPHYR_CYCLE_NAME("zephyr_cycle_name"),
    ZEPHYR_FOLDER("zephyr_folder"),
    ZEPHYR_FOLDER_DESCRIPTION("zephyr_folder_desc"),
    ZEPHYR_API_ACCESS_KEY("zephyr_api_access_key"),
    ZEPHYR_API_SECRET_KEY("zephyr_api_secret_key");

    public final String value;
}
