package utils.api.meeting;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import utils.api.MethodType;
import utils.api.RestHelper;
import utils.api.enums.CommonPath;
import utils.api.meeting.model.Meeting;
import utils.api.meeting.model.request.MeetingRequestBody;
import utils.config.EnvData;

import java.util.UUID;

@Slf4j
public class MeetingApi extends RestHelper {

    public MeetingApi() {
        super(EnvData.getActiveUser(), "tumor-board/api/v1/meeting/");
    }

    public UUID create(MeetingRequestBody requestBody) {
        final var body = getParsedBody(requestBody);
        final var response = sendRequest(MethodType.POST, body).then().statusCode(HttpStatus.SC_CREATED).extract();
        log.info("meeting has been created with name: " + requestBody.getMeetingName());
        final var uuid = response.jsonPath().getUUID(CommonPath.UUID.value);
        user.resources.getMeetingList().add(Meeting.builder().request(requestBody).uuid(uuid).build());
        return uuid;
    }

    public void delete(UUID uuid) {
        endpoint += uuid.toString();
        sendRequest(MethodType.DELETE, "").then().statusCode(HttpStatus.SC_NO_CONTENT);
        log.info("meeting has been removed");
    }
}
