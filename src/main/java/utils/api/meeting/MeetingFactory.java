package utils.api.meeting;

import com.github.javafaker.Faker;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import utils.api.meeting.model.request.Attendee;
import utils.api.meeting.model.request.MeetingRequestBody;
import utils.config.EnvData;

import java.util.Collections;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MeetingFactory {
    private static final String VIDEO_URL = "https://plus.google.com/hangouts";
    private static final String VIDEO_ADDITIONAL_NOTES = "Miscellaneous notes-PHI_NTB_Testkusanbwp199unique identification";
    private static final String PREFIX = "E2E-UI-Java-MEETING-";

    public static MeetingRequestBody getBasicFutureMeeting() {
        final var startDate = DateTime.now().plusDays(1).getMillis();
        final var endDate = DateTime.now().plusDays(2).getMillis();

        final var user = EnvData.getActiveUser();

        return MeetingRequestBody.builder()
                .meetingName(PREFIX + Faker.instance().number().numberBetween(10, 10000))
                .organizationUuids(Collections.singletonList(user.getFacilityId().toString()))
                .meetingLocation("room")
                .meetingStartDateTime(startDate)
                .meetingEndDateTime(endDate)
                .repeatFrequencyId(1)
                .videoUrl(VIDEO_URL)
                .videoAdditionalNotes(VIDEO_ADDITIONAL_NOTES)
                .meetingAttendees(Collections.singletonList(Attendee.builder().attendeeId(user.getOktaId()).build()))
                .meetingFacility(user.getFacilityId().toString())
                .presentationEditable(true)
                .recurrenceDetails(null)
                .recurrent(false)
                .meetingType("Breast")
                .build();
    }
}
