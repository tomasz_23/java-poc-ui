package utils.api.meeting.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Attendee {
    @JsonProperty("attendee_id")
    private final String attendeeId;
}