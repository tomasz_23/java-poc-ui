package utils.api.meeting.model.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class MeetingRequestBody {
    private final String meetingFacility;
    private final long meetingEndDateTime;
    private final String videoAdditionalNotes;
    private final String meetingType;
    private final long meetingStartDateTime;
    private final boolean presentationEditable;
    private final String meetingName;
    private final String videoUrl;
    private final List<Attendee> meetingAttendees;
    private final List<String> organizationUuids;
    private final Object recurrenceDetails;
    private final int repeatFrequencyId;
    private final String meetingLocation;
    private final boolean recurrent;
}