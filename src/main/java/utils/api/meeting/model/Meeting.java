package utils.api.meeting.model;

import lombok.Builder;
import lombok.Getter;
import utils.api.meeting.model.request.MeetingRequestBody;

import java.util.UUID;

@Builder
@Getter
public class Meeting {
    private MeetingRequestBody request;
    private UUID uuid;
}
