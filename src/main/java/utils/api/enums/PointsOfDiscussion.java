package utils.api.enums;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum PointsOfDiscussion {
    NEW_PATIENT("New Patient"),
    TREATMENT("Treatment"),
    OTHER("Other");

    public final String value;

    public static PointsOfDiscussion getRandom() {
        return PointsOfDiscussion.values()[Faker.instance().random().nextInt(PointsOfDiscussion.values().length)];
    }
}
