package utils.api.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CommonPath {
    UUID("uuid");

    public final String value;
}
