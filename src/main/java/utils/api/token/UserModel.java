package utils.api.token;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserModel {
    private final String username;
    private final String password;
}
