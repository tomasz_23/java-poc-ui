package utils.api.token;

import io.restassured.path.json.JsonPath;
import utils.api.MethodType;
import utils.api.RestHelper;
import utils.config.EnvData;
import utils.config.enums.UserType;

public class Token extends RestHelper {
    public Token() {
        super(EnvData.getActiveUser());
    }

    public void generateToken() {
        final var uiConf = getUIConfiguration();
        final var sessionToken = getSession(uiConf);
        final var token = getToken(uiConf, sessionToken);
        EnvData.getUserFromConfig(UserType.findByValue(user.getUserValue())).setToken(token);
    }

    private JsonPath getUIConfiguration() {
        endpoint = "tumor-board/api/v1/ui-configuration";
        return sendRequest(MethodType.GET).body().jsonPath();
    }

    private String getSession(JsonPath uiConf) {
        host = uiConf.getString("platform.oktaUrl");
        endpoint = "api/v1/authn";

        final var userModel = UserModel.builder()
                .username(user.getEmail())
                .password(user.getPassword())
                .build();

        final var body = getParsedBody(userModel);
        return sendRequest(MethodType.POST, body).body().jsonPath().getString("sessionToken");
    }

    private String getToken(JsonPath uiConf, String sessionToken) {
        endpoint = "oauth2/default/v1/authorize";

        final var state = "eyJwcm92aWRlclR5cGUiOiJva3RhIiwicmV0dXJuQnkiOiJwb3N0TWVzc2FnZSJ9";

        params.put("client_id", uiConf.getString("platform.oktaClientId"));
        params.put("response_type", "code");
        params.put("prompt", "none");
        params.put("scope", "openid offline_access profile");
        params.put("state", state);
        params.put("redirect_uri", uiConf.getString("platform.apiGatewayURL") + "/login_v2");
        params.put("sessionToken", sessionToken);

        final var response = sendRequest(MethodType.GET);

        return "Bearer " + response.getHeaders().getList("Set-Cookie").get(0).getValue().split("=")[1].split(";")[0].trim();
    }
}
