package utils.api;

public enum MethodType {
    GET, PUT, POST, DELETE
}
