package utils.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import utils.config.model.User;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.util.Objects.nonNull;

@Slf4j
@NoArgsConstructor
public abstract class RestHelper {
    private final ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
    private final PrintStream requestVar = new PrintStream(requestStream, true);
    private final PrintStream responseVar = new PrintStream(responseStream, true);
    protected String host;
    protected String endpoint;
    protected User user;
    protected Map<String, String> params = new HashMap<>();
    protected Map<String, String> headers = new HashMap<>();
    protected RequestSpecification requestSpecification;

    protected RestHelper(User user, String endpoint) {
        this.user = user;
        this.host = user.getUrl();
        this.endpoint = endpoint;
    }

    protected RestHelper(User user) {
        this.user = user;
        this.host = user.getUrl();
    }

    protected RestHelper(String host) {
        this.host = host;
    }

    @SneakyThrows
    protected String getParsedBody(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        return mapper.writeValueAsString(object);
    }

    protected Response sendRequest(MethodType type, String... body) {
        Response response = null;
        switch (type) {
            case GET:
                response = get();
                break;
            case PUT:
                response = put(body[0]);
                break;
            case POST:
                response = post(body[0]);
                break;
            case DELETE:
                response = delete();
                break;
        }

//        Allure.addAttachment(endpoint + "-request", requestStream.toString());
//        Allure.addAttachment(endpoint + "-response", responseStream.toString());

        return response;
    }

    private RequestSpecification getRequestSpec() {
        var requestSpecBuilder = new RequestSpecBuilder()
                .addFilter(new ResponseLoggingFilter(LogDetail.ALL, responseVar))
                .addFilter(new RequestLoggingFilter(LogDetail.ALL, requestVar))
                .setRelaxedHTTPSValidation()
                .setUrlEncodingEnabled(true)
                .setBaseUri(host)
                .setBasePath(endpoint);


        if (!params.isEmpty()) {
            requestSpecBuilder.addParams(params);
            params.clear();
        }

        if (!headers.isEmpty()) {
            requestSpecBuilder.addHeaders(headers);
        }

        if (nonNull(user) && nonNull(user.getToken())) {
            requestSpecBuilder.addHeader("Authorization", user.getToken());
        }

        requestSpecification = requestSpecBuilder.build();
        return given().spec(requestSpecification);
    }

    private Response post(String parsedBody) {
        return getRequestSpec()
                .contentType(ContentType.JSON)
                .body(parsedBody)
                .post();
    }

    private Response delete() {
        return getRequestSpec()
                .contentType(ContentType.JSON)
                .delete();
    }

    private Response put(String parsedBody) {
        return getRequestSpec()
                .contentType(ContentType.JSON)
                .body(parsedBody)
                .put();
    }

    private Response get() {
        return getRequestSpec()
                .contentType(ContentType.ANY)
                .get();
    }
}
