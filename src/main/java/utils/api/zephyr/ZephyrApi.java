package utils.api.zephyr;

import io.restassured.response.Response;
import utils.api.MethodType;
import utils.api.RestHelper;
import utils.api.zephyr.models.cycles.CycleSearchItem;
import utils.api.zephyr.models.execution.ExecutionRequestBody;
import utils.api.zephyr.models.execution.ExecutionStatus;
import utils.api.zephyr.models.execution.ExecutionUpdateRequestBody;
import utils.api.zephyr.models.folders.get.response.FolderResponse;
import utils.api.zephyr.models.folders.put.request.FolderUpdateRequest;
import utils.api.zephyr.models.issue.request.SearchQuery;
import utils.config.EnvData;
import utils.enums.Properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZephyrApi extends RestHelper {
    private static final String BASE_URL = "https://prod-api.zephyr4jiracloud.com/connect/";
    private static final String V1 = "/public/rest/api/1.0";
    private static final String V2 = "/public/rest/api/2.0";
    private static final String FOLDER_NAME = EnvData.getProperty(Properties.ZEPHYR_FOLDER);
    private static final String PROJECT_ID = EnvData.getProperty(Properties.ZEPHYR_PROJECT_ID);
    private static final String VERSION_ID = EnvData.getProperty(Properties.ZEPHYR_VERSION_ID);
    private static final String FOLDER_DESCRIPTION = EnvData.getProperty(Properties.ZEPHYR_FOLDER_DESCRIPTION);

    public ZephyrApi() {
        host = BASE_URL;
    }

    public List<CycleSearchItem> getCycles() {
        endpoint = V1 + "/cycles/search";

        params.put("projectId", PROJECT_ID);
        params.put("versionId", VERSION_ID);

        final var response = sendZephyrRequest(MethodType.GET);
        return response.jsonPath().getList("", CycleSearchItem.class);
    }

    public FolderResponse getFolder() {
        endpoint = V1 + "/cyclesummary/search";

        params.put("projectId", PROJECT_ID);
        params.put("searchQuery", FOLDER_NAME);

        final var response = sendZephyrRequest(MethodType.GET);
        return response.jsonPath().getObject("", FolderResponse.class);
    }

    public void updateFolder(String folderId, String cycleId) {
        endpoint = V1 + "/folder/" + folderId;

        final var requestBody = FolderUpdateRequest.builder()
                .name(FOLDER_NAME)
                .description(FOLDER_DESCRIPTION)
                .cycleId(cycleId)
                .versionId(VERSION_ID)
                .clearCustomFieldsFlag(true)
                .projectId(PROJECT_ID)
                .build();

        final var body = getParsedBody(requestBody);
        sendZephyrRequest(MethodType.PUT, body).then().statusCode(200);
    }

    public String createExecution(String issueId, String cycleId, String folderId) {
        endpoint = V1 + "/execution";

        final var requestBody = ExecutionRequestBody.builder()
                .projectId(PROJECT_ID)
                .issueId(issueId)
                .cycleId(cycleId)
                .versionId(VERSION_ID)
                .folderId(folderId)
                .build();

        final var body = getParsedBody(requestBody);
        final var response = sendZephyrRequest(MethodType.POST, body);

        return response.jsonPath().getString("execution.id");
    }

    public void updateExecution(String executionId, String issueId, int result) {
        endpoint = V1 + "/execution/" + executionId;

        final var status = ExecutionStatus.builder().id(result).build();

        final var requestBody = ExecutionUpdateRequestBody.builder()
                .projectId(PROJECT_ID)
                .issueId(issueId)
                .comment("Test reported by automated script")
                .versionId(VERSION_ID)
                .status(status)
                .build();

        final var body = getParsedBody(requestBody);
        sendZephyrRequest(MethodType.PUT, body);
    }

    public String searchQuery(String testId) {
        endpoint = V1 + "/zql/search";

        final var body = SearchQuery.generateQuery(testId);
        final var response = sendZephyrRequest(MethodType.POST, getParsedBody(body));

        return response.jsonPath().getString("searchObjectList[0].execution.issueId");
    }

    private void addHeaders(String token) {
        headers.clear();
        headers.put("zapiAccessKey", EnvData.getProperty(Properties.ZEPHYR_API_ACCESS_KEY));
        headers.put("Authorization", token);
    }

    private Response sendZephyrRequest(MethodType methodType, String... body) {
        final var token = JWTGenerate.getToken(methodType, endpoint, getJWTParams());

        addHeaders(token);
        if (body.length <= 0) {
            return sendRequest(methodType, "");
        } else {
            return sendRequest(methodType, body[0]);
        }
    }

    private Map<String, String[]> getJWTParams() {
        Map<String, String[]> map = new HashMap<>();
        params.forEach((key, value) -> map.put(key, new String[]{value}));
        return map;
    }
}
