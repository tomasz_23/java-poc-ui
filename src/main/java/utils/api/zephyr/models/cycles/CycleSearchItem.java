package utils.api.zephyr.models.cycles;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CycleSearchItem {
    private String createdByAccountId;
    private String modifiedByAccountId;
    private String tenantKey;
    private String description;
    private String cycleIndex;
    private String creationDate;
    private String projectCycleVersionIndex;
    private String environment;
    private int versionId;
    private String build;
    private String createdBy;
    private String name;
    private String modifiedBy;
    private String id;
    private int projectId;
}