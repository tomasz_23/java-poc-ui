package utils.api.zephyr.models.folders.get.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExecutionSummariesItem {
    private int executionStatusKey;
    private int count;
    private String executionStatusName;
    private String executionStatusDescription;
    private String executionStatusColor;
}