package utils.api.zephyr.models.folders.get.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FolderResponse {
    private List<FoldersItem> folders;
    private List<Integer> versions;
    private List<CyclesItem> cycles;
}