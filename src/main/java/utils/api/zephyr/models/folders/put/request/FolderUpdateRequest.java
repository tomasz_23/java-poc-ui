package utils.api.zephyr.models.folders.put.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class FolderUpdateRequest {
    private final String name;
    private final String description;
    private final String cycleId;
    private final String versionId;
    private final boolean clearCustomFieldsFlag;
    private final String projectId;
}
