package utils.api.zephyr.models.folders.get.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FoldersItem {
    private long modifiedTime;
    private String createdByAccountId;
    private String modifiedByAccountId;
    private int totalExecutions;
    private String cycleId;
    private String description;
    private String nodeType;
    private String versionId;
    private String createdBy;
    private String name;
    private long createdTime;
    private String modifiedBy;
    private String id;
    private String cycleName;
    private int projectId;
    private List<ExecutionSummariesItem> executionSummaries;
}