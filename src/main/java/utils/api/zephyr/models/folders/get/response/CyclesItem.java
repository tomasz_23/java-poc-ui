package utils.api.zephyr.models.folders.get.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CyclesItem {
    private int totalExecutionTime;
    private String createdByAccountId;
    private int totalExecutions;
    private String tenantKey;
    private String description;
    private String cycleIndex;
    private int totalDefects;
    private String totalLoggedTimeFormatted;
    private String action;
    private String modifiedBy;
    private String id;
    private List<ExecutionSummariesItem> executionSummaries;
    private int totalLoggedTime;
    private String offset;
    private String modifiedByAccountId;
    private int totalExecuted;
    private String creationDate;
    private int totalCycleExecuted;
    private String projectCycleVersionIndex;
    private int executionsAwaitingLog;
    private String environment;
    private int versionId;
    private int totalCycleExecutions;
    private String build;
    private String createdBy;
    private String name;
    private String totalExecutionTimeFormatted;
    private int totalFolders;
    private int projectId;
}