package utils.api.zephyr.models.tc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Builder
public class Test {
    private String testName;
    private String executionId;
    private String tag;
    private String issueId;
    private int result;
    @Builder.Default
    private List<Step> steps = new ArrayList<>();
}
