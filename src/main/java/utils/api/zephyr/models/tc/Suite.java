package utils.api.zephyr.models.tc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Builder
public class Suite {
    private String cycleId;
    private String folderId;
    @Builder.Default
    private List<Test> tests = new ArrayList<>();
}
