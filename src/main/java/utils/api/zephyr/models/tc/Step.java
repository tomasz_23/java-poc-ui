package utils.api.zephyr.models.tc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Step {
    private String name;
    private String screenshot;
    private String errorDescription;
    private int result;
}
