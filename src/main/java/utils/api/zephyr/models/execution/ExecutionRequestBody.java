package utils.api.zephyr.models.execution;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ExecutionRequestBody {
    private String projectId;
    private String issueId;
    private String cycleId;
    private String versionId;
    private String folderId;
}
