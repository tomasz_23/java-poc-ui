package utils.api.zephyr.models.execution;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ExecutionStatus {
    private int id;
}
