package utils.api.zephyr.models.execution;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ExecutionUpdateRequestBody {
    private String projectId;
    private String issueId;
    private String comment;
    private String versionId;
    private ExecutionStatus status;
}
