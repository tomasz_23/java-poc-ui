package utils.api.zephyr.models.issue.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SearchQuery {
    private String zqlQuery;

    public static SearchQuery generateQuery(String testId) {
        return SearchQuery.builder()
                .zqlQuery("ISSUE=" + testId)
                .build();
    }
}
