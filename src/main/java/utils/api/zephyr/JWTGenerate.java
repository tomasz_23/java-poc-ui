package utils.api.zephyr;

import com.atlassian.jwt.SigningAlgorithm;
import com.atlassian.jwt.core.writer.JsonSmartJwtJsonBuilder;
import com.atlassian.jwt.core.writer.JwtClaimsBuilder;
import com.atlassian.jwt.core.writer.NimbusJwtWriterFactory;
import com.atlassian.jwt.httpclient.CanonicalHttpUriRequest;
import com.atlassian.jwt.writer.JwtJsonBuilder;
import com.atlassian.jwt.writer.JwtWriterFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import utils.api.MethodType;
import utils.config.EnvData;
import utils.enums.Properties;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JWTGenerate {

    @SneakyThrows
    public static String getToken(MethodType methodType, String endpoint, Map<String, String[]> hashMap) {
        final var issuedAt = System.currentTimeMillis() / 1000L;
        final var expiresAt = issuedAt + 180L;
        final var key = EnvData.getProperty(Properties.ZEPHYR_API_ACCESS_KEY);
        final var sharedSecret = EnvData.getProperty(Properties.ZEPHYR_API_SECRET_KEY);
        final var contextPath = "/";

        JwtJsonBuilder jwtBuilder = new JsonSmartJwtJsonBuilder()
                .issuedAt(issuedAt)
                .expirationTime(expiresAt)
                .issuer(key)
                .subject("DIS.NavifyAtlassianGenericTester2@roche.com");

        final var canonical = new CanonicalHttpUriRequest(methodType.name(),
                endpoint, contextPath, hashMap);
        JwtClaimsBuilder.appendHttpRequestClaims(jwtBuilder, canonical);

        JwtWriterFactory jwtWriterFactory = new NimbusJwtWriterFactory();

        return "JWT " + jwtWriterFactory.macSigningWriter(SigningAlgorithm.HS256,
                sharedSecret).jsonToJwt(jwtBuilder.build());
    }
}
