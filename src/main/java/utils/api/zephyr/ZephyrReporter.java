package utils.api.zephyr;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import utils.api.zephyr.models.tc.Suite;
import utils.api.zephyr.models.tc.Test;
import utils.config.EnvData;
import utils.enums.Properties;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ZephyrReporter {
    @Getter
    private static Suite suite;
    private static ZephyrApi zephyrApi;

    public static void generateBasicData() {
        zephyrApi = new ZephyrApi();
        final var cycleId = getCycleId();
        final var folderId = getFolderId(cycleId);
        zephyrApi.updateFolder(folderId, cycleId);

        suite = Suite.builder()
                .cycleId(cycleId)
                .folderId(folderId)
                .build();
    }

    public static void reportAll() {
        suite.getTests().forEach(test -> {
            test.setIssueId(zephyrApi.searchQuery(test.getTag()));
            test.setExecutionId(zephyrApi.createExecution(test.getIssueId(), suite.getCycleId(), suite.getFolderId()));
            zephyrApi.updateExecution(test.getExecutionId(), test.getIssueId(), test.getResult());
            log.info(test.getTag() + " has been reported to Zephyr correctly");
        });
    }

    public static void addTestToSuit(Test test) {
        suite.getTests().add(test);
    }

    public static String getTag(String value) {
        return value.split(" ")[0].split("@")[1];
    }

    private static String getCycleId() {
        return zephyrApi.getCycles().stream()
                .filter(cycle -> EnvData.getProperty(Properties.ZEPHYR_CYCLE_NAME).equals(cycle.getName()))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Cycle has not been found"))
                .getId();
    }

    private static String getFolderId(String cycleId) {
        return zephyrApi.getFolder().getFolders().stream()
                .filter(folder -> EnvData.getProperty(Properties.ZEPHYR_VERSION_ID).equals(folder.getVersionId()))
                .filter(folder -> folder.getCycleId().equals(cycleId))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Folder has not been found"))
                .getId();
    }
}
