package utils.api.patient.model.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Builder
@Getter
@Setter
public class PatientRequestBody {
    private final String firstName;
    private final String lastName;
    private final String versionId;
    private final String gender;
    private final String dateOfBirth;
    private final UUID facility;
    private final Object contacts;
    private final List<PatientIdentifiersItem> patientIdentifiers;
}