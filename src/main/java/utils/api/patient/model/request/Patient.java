package utils.api.patient.model.request;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class Patient {
    private PatientRequestBody request;
    private UUID uuid;
}
