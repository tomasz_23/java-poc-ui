package utils.api.patient.model.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Builder
@Getter
@Setter
public class PatientIdentifiersItem {
    private final String system;
    private final String facility;
    private final UUID value;
}