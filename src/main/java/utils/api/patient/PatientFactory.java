package utils.api.patient;

import com.github.javafaker.Faker;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import utils.api.patient.model.request.PatientIdentifiersItem;
import utils.api.patient.model.request.PatientRequestBody;
import utils.config.EnvData;

import java.util.Arrays;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PatientFactory {
    private static final String PREFIX = "E2E-UI-Java-PATIENT-";

    public static PatientRequestBody getBasicPatient() {
        final var mrn = UUID.randomUUID();
        final var patientIdentifier1 = PatientIdentifiersItem.builder()
                .facility("ce5f164c-ae0f-11e7-9220-c7137b83fb6a")
                .system("http://dip.roche.com/context-identifier/impacted-patient-identifier/assigner-UHC/UHC")
                .value(mrn)
                .build();

        final var patientIdentifier2 = PatientIdentifiersItem.builder()
                .facility("ce5f164c-ae0f-11e7-9220-c7137b83fb6a")
                .system("http://dip.roche.com/context-identifier/impacted-patient-identifier/assigner-UHC/UEI")
                .value(mrn)
                .build();

        return PatientRequestBody.builder()
                .firstName(PREFIX + Faker.instance().name().firstName())
                .lastName(PREFIX + Faker.instance().name().lastName())
                .dateOfBirth("2000-10-10T00:00:00+02:00")
                .facility(EnvData.getActiveUser().getFacilityId())
                .gender("M")
                .patientIdentifiers(Arrays.asList(patientIdentifier1, patientIdentifier2))
                .build();
    }
}
