package utils.api.patient;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import utils.api.MethodType;
import utils.api.RestHelper;
import utils.api.enums.CommonPath;
import utils.api.patient.model.request.Patient;
import utils.api.patient.model.request.PatientRequestBody;
import utils.config.EnvData;

import java.util.UUID;

@Slf4j
public class PatientApi extends RestHelper {

    public PatientApi() {
        super(EnvData.getActiveUser(), "tumor-board/api/v1/patient/");
    }

    public UUID create(PatientRequestBody requestBody) {
        final var body = getParsedBody(requestBody);
        final var response = sendRequest(MethodType.POST, body).then().statusCode(HttpStatus.SC_CREATED).extract();
        log.info("patient has been created with mrn: " + requestBody.getPatientIdentifiers().get(0).getValue());

        final var uuid = response.jsonPath().getUUID(CommonPath.UUID.value);
        user.resources.getPatientList().add(Patient.builder().request(requestBody).uuid(uuid).build());
        return uuid;
    }

    public void delete(UUID uuid) {
        endpoint += uuid.toString();
        sendRequest(MethodType.DELETE, "").then().statusCode(HttpStatus.SC_NO_CONTENT);
        log.info("patient has been removed");
    }
}
