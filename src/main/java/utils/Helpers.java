package utils;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static org.awaitility.Awaitility.with;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Helpers {

    public static void waitFor(Callable<Boolean> condition) {
        with().pollInterval(3, TimeUnit.SECONDS)
                .await()
                .atMost(30, TimeUnit.SECONDS)
                .until(condition);
    }

    public static SelenideElement findElementByContainText(ElementsCollection list, Predicate<SelenideElement> predicate) {
        return list.stream()
                .filter(predicate)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("List doesn't have " + predicate));
    }

    public static Predicate<SelenideElement> byValue(String name) {
        return element -> element.getText().contains(name);
    }
}
