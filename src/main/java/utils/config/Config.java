package utils.config;

import lombok.Getter;
import utils.config.model.User;

import java.util.List;

@Getter
public class Config {
    private List<User> users;
    private String baseUrl;
    private String supportToolUrl;
    private String appsUrl;
}
