package utils.config.model;

import lombok.Getter;
import lombok.Setter;
import utils.config.enums.Role;

import java.util.UUID;

@Getter
public class User {
    public final Resources resources = new Resources();
    private String userValue;
    private String email;
    private String password;
    private String name;
    private String oktaId;
    private String facility;
    private UUID facilityId;
    private String tenantAlias;
    private Role role;
    @Setter
    private String url;
    @Setter
    private String token = null;
}
