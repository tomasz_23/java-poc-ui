package utils.config.model;

import lombok.Getter;
import utils.api.meeting.model.Meeting;
import utils.api.patient.model.request.Patient;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Resources {
    private final List<Patient> patientList = new ArrayList<>();
    private final List<Meeting> meetingList = new ArrayList<>();

}
