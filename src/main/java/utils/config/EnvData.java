package utils.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import utils.api.token.Token;
import utils.config.enums.UserType;
import utils.config.model.User;
import utils.enums.Properties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class EnvData {

    @Getter
    private static final List<User> users = new ArrayList<>();
    @Getter
    private static Config config;

    @Getter
    private static User activeUser;

    static {
        final var mapper = new ObjectMapper(new YAMLFactory());
        final var env = getProperty(Properties.ENV);

        if (env == null) {
            log.error("You have to add -Denv property in maven command");
        } else {
            try {
                config = mapper.readValue(new File(System.getProperty("user.dir") + "/src/main/resources/configs/" + env.toLowerCase() + ".yaml"), Config.class);
            } catch (IOException e) {
                log.error("Config hasn't been found");
            }
        }
    }

    public static void setActiveUser(User user) {
        activeUser = user;
    }

    public static void setActiveUser(UserType userType) {
        activeUser = getUserFromConfig(userType);
        if (!nonNull(activeUser.getToken())) {
            new Token().generateToken();
        }
    }

    public static User getUserFromConfig(UserType userType) {
        if (!userListContains(userType)) {
            var user = config.getUsers().stream()
                    .filter(u -> u.getUserValue().equals(userType.value))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("User type " + userType.value + " has not been found"));

            final var toReplace = getBaseUrl().split("\\.")[0];
            user.setUrl("https://" + getBaseUrl().replace(toReplace, user.getTenantAlias()));
            users.add(user);
            return user;
        } else {
            return getUserFromList(userType);
        }
    }

    public static String getBaseUrl() {
        return config.getBaseUrl();
    }

    public static String getSupportToolUrl() {
        return config.getSupportToolUrl();
    }

    public static boolean getZephyrEnable() {
        var integration = false;

        final var enabled = getProperty(Properties.ZEPHYR_ENABLED);
        if (nonNull(enabled)) {
            integration = Boolean.parseBoolean(enabled);
        }

        return integration;
    }

    public static String getProperty(Properties properties) {
        final var property = System.getProperty(properties.value);
        if (!nonNull(property) || property.equals("")) {
            log.error(properties.value + " param has not been added or his value is null");
            System.exit(2);
        }
        return property;
    }

    private static boolean userListContains(UserType userType) {
        return users.stream().anyMatch(u -> u.getUserValue().equals(userType.value));
    }

    private static User getUserFromList(UserType userType) {
        return users.stream()
                .filter(u -> u.getUserValue().equals(userType.value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("user not found"));
    }
}
