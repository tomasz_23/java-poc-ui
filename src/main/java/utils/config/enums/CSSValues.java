package utils.config.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CSSValues {
    COLOR("color"),
    FILL("fill");

    public final String value;
}
