package utils.config.enums;

import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum UserType {
    LOGIN("login"),
    LOGIN_UCSF("loginUCSF"),
    LOGIN_CORRECT_ADDRESS("loginCorrectAddress"),
    LOGIN_APPS_DEFAULT_USER("loginAppsDefaultUser"),
    LOGIN_DISABLED_APPS("loginDisabledApps");

    public final String value;

    public static UserType findByValue(String value) {
        return Arrays.stream(UserType.values())
                .filter(l -> l.value.contains(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(value + "has not been found"));
    }
}
