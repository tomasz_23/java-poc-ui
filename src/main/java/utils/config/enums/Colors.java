package utils.config.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Colors {
    WHITE("rgba(0, 0, 0, 0.87)"),
    BLUE_HOVER("rgba(0, 102, 204, 1)"),
    BLUE_PINNED("rgb(0, 102, 204)");

    public final String color;
}
