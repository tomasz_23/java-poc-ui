package utils.config.enums;

public enum Role {
    DEFAULT_USER, CUSTOMER_ADMIN, VIEW_ONLY_USER
}
