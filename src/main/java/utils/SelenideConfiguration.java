package utils;

import com.codeborne.selenide.Configuration;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.PageLoadStrategy;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SelenideConfiguration {
    public static void setUp() {
        Configuration.pageLoadStrategy = PageLoadStrategy.NORMAL.toString();
        Configuration.pageLoadTimeout = 40000;
//        Configuration.browser="Edge";
        Configuration.timeout = 20000;
        Configuration.browserSize = "1920x1080";
        Configuration.downloadsFolder = "target/selenide/downloads";
//        Configuration.headless = true;
        Configuration.reportsFolder = "target/selenide/";
        Configuration.savePageSource = false;
        Configuration.screenshots = false;
    }
}
