package pages;

import components.Abstract;

import static com.codeborne.selenide.Selenide.$;

public class CancerInfo extends Abstract {

    public CancerInfo() {
        super($("app-patient-cancer-info-view"));
    }
}