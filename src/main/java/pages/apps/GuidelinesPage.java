package pages.apps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import pages.apps.dropdownlist.CancerType;
import pages.apps.dropdownlist.DropDown;
import pages.apps.dropdownlist.GuidelineSource;
import pages.apps.dropdownlist.StartingPoint;
import pages.apps.help_and_more.AnalyticsPage;
import utils.enums.GuidelineDropdownList;
import utils.enums.HelpAndMore;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class GuidelinesPage extends Abstract {
    public final SelenideElement helpAndMore = component.$(".mat-button--help");
    public final ElementsCollection helpAndMoreOptions = $$(".mat-menu-content a");

    public final SelenideElement gdlNodes = component.$("gdl-nodes-list , .nodes");
    public final ElementsCollection pathContainers = gdlNodes.$$("gdl-content");

    public final ElementsCollection clinicalPathwayList = component.$$(".gdl-clinical-path button span");

    public final SelenideElement resetIcon = component.$("[data-mat-icon-name='reset']");

    public final GuidelineOverview guidelineOverview = new GuidelineOverview();

    public final GuidelineSource guidelineSource = new GuidelineSource();
    public final CancerType cancerType = new CancerType();
    public final StartingPoint startingPoint = new StartingPoint();

    public GuidelinesPage() {
        super($("gdl-generic-view"));
    }

    public void selectAndGetNode(int index) {
        final var pathwayValue = pathContainers.get(index).$("p").getText();
        final var indexOnClinicalPathway = clinicalPathwayList.size() - 1;

        pathContainers.get(indexOnClinicalPathway).click();
        clinicalPathwayList.get(indexOnClinicalPathway).shouldHave(Condition.text(pathwayValue));
    }

    public AnalyticsPage openAnalytics() {
        findElementByContainText(helpAndMoreOptions, byValue(HelpAndMore.ANALYTICS.value)).click();
        return new AnalyticsPage();
    }

    public void fillDropdownByScroll(GuidelineDropdownList guidelineDropdownList, DropDown value) {
        $(".loader").shouldBe(disappear);
        switch (guidelineDropdownList) {
            case SOURCE:
                guidelineSource.selectWithScroll(value);
                break;
            case CANCER_TYPE:
                cancerType.selectWithScroll(value);
                break;
            case STARTING_POINT:
                startingPoint.selectWithScroll(value);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + guidelineDropdownList);
        }
    }
}
