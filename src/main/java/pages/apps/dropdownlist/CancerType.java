package pages.apps.dropdownlist;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AllArgsConstructor;
import pages.apps.SearchApp;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static java.util.Objects.nonNull;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class CancerType implements SearchApp {
    private final ElementsCollection options = $$(".ps-content .mat-menu-item");
    private final SelenideElement sourceSelectBox = $("[label='Cancer Type:']");
    private final SelenideElement selectedValue = sourceSelectBox.$(".mat-select-value-text");

    @Override
    public boolean hasOption(DropDown source) {
        return nonNull(findElementByContainText(options, byValue(source.getValue())));
    }

    @Override
    public void selectWithScroll(DropDown source) {
        sourceSelectBox.click();
        findElementByContainText(options, byValue(source.getValue())).click();
    }

    public void waitForValueDisplayed(DropDown dropDown) {
        selectedValue.shouldBe(text(dropDown.getValue()));
    }

    @AllArgsConstructor
    public enum Values implements DropDown {
        BREAST_CANCER("Breast Cancer"),
        COLON_CANCER("Colon Cancer"),
        TEST_GUIDELINE("Test Guideline");

        private final String value;

        @Override
        public String getValue() {
            return value;
        }
    }
}
