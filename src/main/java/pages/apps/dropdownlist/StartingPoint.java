package pages.apps.dropdownlist;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AllArgsConstructor;
import org.openqa.selenium.By;
import pages.apps.SearchApp;

import static com.codeborne.selenide.Selenide.$;
import static java.util.Objects.nonNull;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class StartingPoint implements SearchApp {
    private final SelenideElement content = $(".select-scrollbar");
    private final ElementsCollection options = content.$$(".mat-option");
    private final SelenideElement sourceSelectBox = $("[label='Starting Point:']");

    @Override
    public boolean hasOption(DropDown source) {
        return nonNull(findElementByContainText(options, byValue(source.getValue())));
    }

    @Override
    public void selectWithScroll(DropDown source) {
        sourceSelectBox.click();
        content.shouldBe(Condition.visible);
        findElementByContainText(options, byValue(source.getValue())).click();
    }

    public void selectByTyping(String value) {
        sourceSelectBox.click();
        content.shouldBe(Condition.visible);
        sourceSelectBox.$(By.cssSelector("input")).setValue(value);
        findElementByContainText(options, byValue(value)).click();
    }

    @AllArgsConstructor
    public enum Values implements DropDown {
        CLINICAL_STAGE_WORKUP("Clinical Stage, Workup"),
        DIAG_1("DIAG-1"),
        SURVEILLANCE("Surveillance");

        private final String value;

        @Override
        public String getValue() {
            return value;
        }
    }
}
