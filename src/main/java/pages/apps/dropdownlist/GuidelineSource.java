package pages.apps.dropdownlist;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AllArgsConstructor;
import pages.apps.SearchApp;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static java.util.Objects.nonNull;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class GuidelineSource implements SearchApp {
    private final ElementsCollection options = $$(".cdk-overlay-container .mat-option");
    private final SelenideElement sourceSelectBox = $("[label='Guideline Source:'] mat-select");
    private final SelenideElement selectedValue = sourceSelectBox.$(".mat-select-value-text");

    @Override
    public boolean hasOption(DropDown source) {
        return nonNull(findElementByContainText(options, byValue(source.getValue())));
    }

    @Override
    public void selectWithScroll(DropDown source) {
        sourceSelectBox.click();
        findElementByContainText(options, byValue(source.getValue())).click();
    }

    public void waitForValueDisplayed(DropDown dropDown) {
        selectedValue.shouldBe(text(dropDown.getValue()));
    }

    @AllArgsConstructor
    public enum Values implements DropDown {
        NCCN("NCCN Guidelines"),
        CLINICAL_PATHWAYS("Clinical Pathways");

        private final String value;

        @Override
        public String getValue() {
            return value;
        }
    }
}
