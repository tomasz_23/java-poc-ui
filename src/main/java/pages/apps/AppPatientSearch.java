package pages.apps;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class AppPatientSearch extends Abstract {
    private final SelenideElement input = component.$("ng-select input");
    private final ElementsCollection options = component.$$(".patient-search__option");

    public AppPatientSearch() {
        super($("app-patient-search"));
    }

    public void select(String value) {
        input.sendKeys(value);
        getPatient(value).scrollTo().click();
    }

    private SelenideElement getPatient(String value) {
        options.shouldBe(sizeGreaterThan(0));
        return findElementByContainText(options, byValue(value));
    }
}
