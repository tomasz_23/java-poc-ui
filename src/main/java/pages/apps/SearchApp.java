package pages.apps;

import pages.apps.dropdownlist.DropDown;

public interface SearchApp {
    boolean hasOption(DropDown source);
    void selectWithScroll(DropDown source);
}
