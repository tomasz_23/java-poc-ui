package pages.apps.help_and_more;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import org.openqa.selenium.By;
import utils.enums.CancerTypePathway;
import utils.enums.Customization;

import java.util.Objects;
import java.util.stream.IntStream;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class AnalyticsPage extends Abstract {
    public final SelenideElement barChart = component.$("gdl-bar-chart");
    public final ElementsCollection gdlAnalyticsPathwaysByCancerListElements = $$(By.xpath("(//*[name()='svg']//*[@class='yAxis'])[1]//*[@class='tick']//*[name()='text']"));

    public final ElementsCollection rect = component.$$(".layer .rect");
    public final SelenideElement tooltip = $(".analytics .d3js-tooltip");

    public final SelenideElement filtersComponent = component.$("gdl-global-filters");

    public final SelenideElement utilizationNumber = component.$(".gdl-analytics-total");
    public final SelenideElement customizedPathwayReasonSection = component.$("gdl-customized-pathway-reason");
    public final SelenideElement clinicalPathwaysByCancerTypeSection = component.$(".gdl-analytics-usage");
    public final SelenideElement pathwaysCustomizationSection = component.$("gdl-guidelines-deviation");

    public AnalyticsPage() {
        super($("div.gdl-analytics"));
    }

    public int getValueFromTooltip(CancerTypePathway cancerTypePathway, Customization customization) {
        final var index = getIndexFindingCancerType(cancerTypePathway);
        getElementToHover(customization, index).hover();

        final var tooltipValue = tooltip.getText().split("\\(")[1].split("\\)")[0];
        return Integer.parseInt(tooltipValue);
    }

    private SelenideElement getElementToHover(Customization customization, int index) {
        return $(By.xpath("(//*[@class='layer'])[" + customization.value + "]/*[@class='rect'][" + index + "]"));
    }

    private int getIndexFindingCancerType(CancerTypePathway cancerTypePathway) {
        return IntStream.range(0, gdlAnalyticsPathwaysByCancerListElements.size())
                .filter(i -> Objects.equals(gdlAnalyticsPathwaysByCancerListElements.get(i).getAttribute("data-test"), cancerTypePathway.value))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("List doesn't have " + cancerTypePathway.value)) + 1;
    }
}
