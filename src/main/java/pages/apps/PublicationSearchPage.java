package pages.apps;

import components.Abstract;

import static com.codeborne.selenide.Selenide.$;

public class PublicationSearchPage extends Abstract {
    public PublicationSearchPage() {
        super($("gdl-generic-view"));
    }
}
