package pages.apps;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.modals.ConfirmationModal;

import static com.codeborne.selenide.Selenide.$;

public class GuidelineOverview extends Abstract {
    public final SelenideElement pinIcon = component.$("[data-mat-icon-name*='pin']");
    private final ElementsCollection stepList = component.$$("gdl-step-node");

    protected GuidelineOverview() {
        super($("gdl-overview"));
    }

    public String clickStepNodeAndGetName(int index) {
        final var step = stepList.get(index);
        final var name = step.getText();
        step.click();
        return name;
    }

    public ConfirmationModal clickPinIconFirstTime() {
        pinIcon.click();
        return new ConfirmationModal();
    }
}
