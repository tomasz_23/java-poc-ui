package pages.apps;

import components.Abstract;

import static com.codeborne.selenide.Selenide.$;

public class ClinicalTrialMatchPage extends Abstract {
    public ClinicalTrialMatchPage() {
        super($("gdl-generic-view"));
    }
}
