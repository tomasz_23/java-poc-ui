package pages;

import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.SelectAppComponent;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PatientMenu extends Abstract {
    public final SelectAppComponent appComponent = new SelectAppComponent(component, By.cssSelector(".label"));
    public final SelenideElement cancerInfo = component.$("[data-e2e-label='patient-menu-cancer-info-link']");
    public final SelenideElement patientHistory = component.$("[data-e2e-label='patient-menu-cancer-history-link']");

    public PatientMenu() {
        super($("app-patient-menu"));
    }
}