package pages;

import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.SlideContent;

import static com.codeborne.selenide.Selenide.$;

public class FullScreenPreview extends Abstract {
    public final SlideContent slideContent = new SlideContent(component);
    public final SelenideElement close = component.$(".fullscreen-preview-component__icon-arrow-back");

    public FullScreenPreview() {
        super($("app-fullscreen-preview"));
    }
}
