package pages;

import com.codeborne.selenide.SelenideElement;
import components.Abstract;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends Abstract {
    public final SelenideElement email = component.$("[data-e2e-label='identification_username']");
    public final SelenideElement continueButton = component.$("[data-e2e-label='identification_submit_button']");

    public LoginPage() {
        super($("auth-ui-layout-login"));
    }
}
