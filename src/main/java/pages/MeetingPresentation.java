package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.SlideContent;

import static com.codeborne.selenide.Condition.visible;

public class MeetingPresentation extends Abstract {
    public final SelenideElement prevSlide = component.$(".meeting-presentation__nav__icon--prev");
    public final SelenideElement nextSlide = component.$(".meeting-presentation__nav__icon--next");
    public final SelenideElement recommendation = component.$(".meeting-presentation-sidebar__link-recommendation");
    public final SelenideElement close = component.$(".meeting-presentation-sidebar__link-exit , .meeting-preparation__back");
    public final ElementsCollection slides = component.$$("[data-e2e-label='meeting-presentation-sidebar-slides-slide']");
    private final SelenideElement slideNavigation = component.$(".meeting-presentation-sidebar__link-slides");

    public MeetingPresentation(SelenideElement element) {
        super(element);
        component.shouldBe(visible);
    }

    public MeetingPresentation expandSlideNavigation() {
        slideNavigation.click();
        return new MeetingPresentation(component);
    }

    public SlideContent selectSlide(int slide) {
        slides.get(slide).click();
        return new SlideContent(component);
    }
}
