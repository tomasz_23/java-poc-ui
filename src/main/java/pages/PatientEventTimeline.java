package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;
import static utils.Helpers.byValue;
import static utils.Helpers.findElementByContainText;

public class PatientEventTimeline extends Abstract {
    public final SelenideElement addTimelineItem = component.$(".patient-event-timeline__headline__link");
    private final ElementsCollection timelineList = component.$$(".patient-event-timeline-view__item");

    public PatientEventTimeline() {
        super($("app-patient-event-timeline"));
    }

    public PatientEventTimelineDetails openTimelineDetails(String name) {
        timelineList.shouldBe(CollectionCondition.sizeGreaterThan(0));
        getTimeline(name).click();
        return new PatientEventTimelineDetails();
    }

    public Timeline getTimelinePTO(String name) {
        final var timeline = getTimeline(name);
        return Timeline.builder()
                .name(timeline.$(".patient-event-timeline-view__item-headline").getText())
                .type(timeline.$(".patient-event-timeline-view__item-type").getText())
                .date(timeline.$(".patient-event-timeline-view__item-date").getText())
                .build();
    }

    private SelenideElement getTimeline(String name) {
        return findElementByContainText(timelineList, byValue(name));
    }

    @Builder
    @Getter
    @AllArgsConstructor
    public static class Timeline {
        private String name;
        private String type;
        private String date;
    }
}