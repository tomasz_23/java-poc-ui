package pages;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import components.Abstract;
import components.SelectAppComponent;
import org.openqa.selenium.By;
import pages.apps.AppPatientSearch;
import utils.api.patient.model.request.PatientRequestBody;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class HeadBar extends Abstract {
    public final SelenideElement userName = component.$(".headbar__user-name");
    public final SelenideElement logoutButton = component.$("[data-e2e-label='headbar-dropdown-logout']");
    public final SelenideElement appsForm = component.$("[data-e2e-label='headbar-nav-apps']");
    public final SelectAppComponent appComponent = new SelectAppComponent(component, By.cssSelector("nap-launchpad-button button div.icon"));
    private final SelenideElement searchButton = component.$(".headbar__search-button");

    public HeadBar() {
        super($("app-headbar"));
    }

    public void logout() {
        WebDriverRunner.getWebDriver().switchTo().defaultContent();
        userName.click();
        logoutButton.click();
        WebDriverRunner.getWebDriver().switchTo().frame($("iframe[src*='/login']"));
        new LoginPage().continueButton.shouldBe(visible);
    }

    public CancerInfo searchPatient(PatientRequestBody patient) {
        searchButton.click();
        new AppPatientSearch().select(patient.getPatientIdentifiers().get(0).getValue().toString());
        return new CancerInfo();
    }

    public HeadBar openAppsForm() {
        appsForm.click();
        return this;
    }
}
