package pages;

import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.MeetingAttachments;
import utils.enums.MeetingAttachmentType;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class PatientEventTimelineDetails extends Abstract {
    public final SelenideElement attachmentsSection = component.$("app-meeting-patient-attachments");
    public final MeetingAttachments gdlAttachments = new MeetingAttachments(component, MeetingAttachmentType.GDL);
    public final MeetingAttachments pubAttachments = new MeetingAttachments(component, MeetingAttachmentType.PUB);
    public final MeetingAttachments ctmAttachments = new MeetingAttachments(component, MeetingAttachmentType.CTM);
    private final SelenideElement editPresentation = component.$("[data-e2e-label='edit-presentation-action']");
    private final SelenideElement viewPresentation = component.$("[data-e2e-label='view-presentation-action']");

    public PatientEventTimelineDetails() {
        super($("app-patient-meeting-timeline-event"));
        component.shouldBe(visible);
    }

    public MeetingPresentation clickEditPresentation() {
        editPresentation.click();
        return new MeetingPresentation($("app-meeting-preparation"));
    }

    public MeetingPresentation clickViewPresentation() {
        viewPresentation.click();
        return new MeetingPresentation($("app-meeting-presentation"));
    }
}