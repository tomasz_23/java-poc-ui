package components;

import com.codeborne.selenide.SelenideElement;

public class Abstract {
    public SelenideElement component;

    public Abstract(SelenideElement component) {
        this.component = component;
    }
}
