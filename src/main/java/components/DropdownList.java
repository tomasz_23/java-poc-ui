package components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class DropdownList extends Abstract {
    public final SelenideElement input = component.$("input");
    public final ElementsCollection options = component.$$(".ng-option");

    public DropdownList(String component) {
        super($(component));
    }

    public void select(String name) {
        input.click();
        options.find(text(name)).click();
        new Actions(WebDriverRunner.getWebDriver()).sendKeys(Keys.ESCAPE).perform();
    }
}
