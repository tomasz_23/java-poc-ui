package components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import pages.apps.ClinicalTrialMatchPage;
import pages.apps.GuidelinesPage;
import pages.apps.PublicationSearchPage;

public class SelectAppComponent {
    private final ElementsCollection apps;

    public SelectAppComponent(SelenideElement component, By locator) {
        apps = component.$$(locator);
    }

    public ClinicalTrialMatchPage goToCTM() {
        apps.get(0).click();
        return new ClinicalTrialMatchPage();
    }

    public PublicationSearchPage goToPS() {
        apps.get(1).click();
        return new PublicationSearchPage();
    }

    public GuidelinesPage goToGDL() {
        apps.get(2).click();
        return new GuidelinesPage();
    }
}
