package components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import pages.FullScreenPreview;
import utils.enums.MeetingAttachmentType;

public class MeetingAttachments extends Abstract {
    public final ElementsCollection attachments;

    public MeetingAttachments(SelenideElement component, MeetingAttachmentType meetingAttachmentType) {
        super(component);
        attachments = component.$$x(".//*[contains(@class,'" + meetingAttachmentType.value + "')]//ancestor::div[@class='meeting-patient-attachments__thumbnail-container']");
    }

    public void clickPinIcon() {
        attachments.first().$(".pin-button__pinned");
    }

    public FullScreenPreview openGdlPreview() {
        attachments.first().click();
        return new FullScreenPreview();
    }
}
