package components;

import com.codeborne.selenide.SelenideElement;

public class ModalButtons extends Abstract {
    public final SelenideElement submit = component.$("[variant*='secondary']");
    public final SelenideElement cancel = component.$("[variant*='default']");

    public ModalButtons(SelenideElement component) {
        super(component);
    }
}
