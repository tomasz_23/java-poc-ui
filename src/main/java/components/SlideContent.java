package components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byCssSelector;

public class SlideContent extends Abstract {
    public final SelenideElement contentPreview = component.$("ce-guidelines-clinical-path");
    public final ElementsCollection nodes = contentPreview.shadowRoot().findAll(byCssSelector("gdl-clinical-path-history-node"));

    public SlideContent(SelenideElement selenideElement) {
        super(selenideElement);
        contentPreview.shouldBe(visible);
    }
}
