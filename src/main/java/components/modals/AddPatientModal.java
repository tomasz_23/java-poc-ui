package components.modals;

import com.codeborne.selenide.SelenideElement;
import com.github.javafaker.Faker;
import components.Abstract;
import components.DropdownList;
import components.ModalButtons;
import utils.api.enums.PointsOfDiscussion;

import static com.codeborne.selenide.Selenide.$;

public class AddPatientModal extends Abstract {
    public final DropdownList meeting = new DropdownList("[e2elabel='meeting-patient-add-meeting-field']");
    public final DropdownList pointOfDiscussion = new DropdownList("[e2elabel='meeting-patient-add-pointOfDiscussion-field']");

    public final ModalButtons buttons = new ModalButtons(component.$(".meeting-patient-add__buttons"));

    public final SelenideElement additionalNotes = $("[e2elabel='meeting-patient-add-additionalNotes-field'] textarea");

    public AddPatientModal() {
        super($("[name='addMeetingPatientModal']"));

    }

    public void fillForm(String name) {
        meeting.select(name);
        pointOfDiscussion.select(PointsOfDiscussion.getRandom().value);
        additionalNotes.setValue(Faker.instance().chuckNorris().fact());
        buttons.submit.click();
        new AddPatientModal();
    }
}
