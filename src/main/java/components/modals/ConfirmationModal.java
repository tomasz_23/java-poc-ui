package components.modals;

import com.codeborne.selenide.SelenideElement;
import components.Abstract;
import components.ModalButtons;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;

public class ConfirmationModal extends Abstract {
    public final SelenideElement header = component.$("h1");
    public final SelenideElement content = component.$("[data-e2e-label='confirmation-dialog-message']");
    public final ModalButtons buttons = new ModalButtons(component.$(".dia-confirmation-dialog__footer"));

    public ConfirmationModal() {
        super($("[name='confirmation-modal'] .dia-confirmation-dialog__body"));
        switchTo().defaultContent();
        component.shouldBe(visible);
    }
}
