package components.modals;

import com.codeborne.selenide.ElementsCollection;
import components.Abstract;
import components.ModalButtons;

import static com.codeborne.selenide.Selenide.$;

public class PinModal extends Abstract {
    public final ElementsCollection meetingList = component.$$(".pin-modal__popup-meeting-name");
    public final ModalButtons buttons = new ModalButtons(component.$(".pin-modal__popup-footer"));

    public PinModal() {
        super($("[name='pinModal']"));
    }

    public void clickSave() {
        buttons.submit.click();
    }
}
