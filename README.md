## Run test by specific suite by command:

- `mvn clean test -DsuiteXmlFile=testng.xml -Denv=APPSQA -Dzephyr_enabled=false`

The default browser is Chrome, if you want to use other browser to run tests, you just add param
e.g. `-Dselenide.browser=edge `

## Zephyr Reporting

To correct reporting to Zephyr a few variables should be added to maven command:

- `mvn clean test -DsuiteXmlFile=suites/apps/pinning.xml -Denv=APPSQA -Dzephyr_enabled=true -Dzephyr_project_id={id} -Dzephyr_version_id={id} -Dzephyr_cycle_name={name} -Dzephyr_folder={name} -Dzephyr_folder_desc={name} -Dzephyr_api_access_key={access_key} -Dzephyr_api_secret_key={secret_key}`